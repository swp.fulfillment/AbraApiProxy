# XML Feed Generator

Acts as XML feed provider. 

XML is generated on page load. Data are retrieved directly from WMS every time, page is refreshed.

Designed to be deployed as docker container.
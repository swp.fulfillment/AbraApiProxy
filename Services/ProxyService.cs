﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text;
using System;
using System.Threading.Tasks;
using System.Linq;
using SwP.Fulfillment.AbraApiProxy.Models;
using SwP.Fulfillment.AbraApiProxy.Models.Extensions;

namespace SwP.Fulfillment.AbraApiProxy.Services
{
    public interface IProxyService
    {
        Task<string> GetIdFromApikey(string apikey);

        Task<SimpleStockCard2[]> GetStockCards(string firmId);
        Task<DrMaxFeed> GetStockCardsAsXML(string firmId);
    }

    internal class ProxyService : BaseAbraWmsService, IProxyService
    {
        /// <summary>The logger</summary>
        private readonly ILogger<ProxyService> _logger;
        public ProxyService(IConfiguration configuration, ILogger<ProxyService> logger) : base(configuration)
        {
            _logger = logger;
        }
        public async Task<string> GetIdFromApikey(string apikey)
        {
            var requestUri = $"firms";
            var select = $"?where=x_apikey+eq+'{apikey}'&select=id";
            requestUri += select;

            HttpResponseMessage response = await _client.GetAsync(requestUri);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError($"Request failed. ABRA response: {response.StatusCode}");
                return null;
            }
            var firms = await response.Content.ReadAsAsync<AbraFirmId[]>();

            if (!firms.Any())
            {
                _logger.LogError("No records returned from ABRA");
                return null;
            }

            return firms[0]?.Id;

        }

        public async Task<SimpleStockCard2[]> GetStockCards(string firmId)
        {

            var requestUri = $"query";

            var json = "{" +
                       " \"class\": \"storesubcards\"," +
                       " \"select\": [" +
                       "   \"store_id\"," +
                       "   \"storecard_id\"," +
                       "   \"store_id.Code\"," +
                       "   \"storecard_id.Code\"," +
                       "   \"storecard_id.Name\"," +
                       "   \"storecard_id.mainunitcode\"," +
                       "    {" +
                       "     \"name\": \"Reservations\"," +
                       "       \"value\": {" +
                       "       \"class\":\"reservations\"," +
                       "       \"select\": [{ \"name\": \"ReservedQTY\", \"value\": \"Sum(reserved)-Sum(supplied)\" }]," +
                       "       \"where\":\"storecard_id eq :storecard_id\"" +
                       "      }" +
                       "    }," +
                       "   \"Quantity\"" +
                       " ]," +
                      $" \"where\": \"store_id eq '2000000101' and storecard_id.hidden eq false and storecard_id.x_firm_id eq '{firmId}'\"" +
                       "}";


            //_logger.LogDebug("Sending request : " + json);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(requestUri, httpContent);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError($"Request failed. ABRA response: {response.StatusCode}");
                return null;
            }
            try
            {
                var cards = await response.Content.ReadAsAsync<AbraQueryResponseItem[]>();
                return cards.Select(x => SimpleStockCardExtension.FromAbraQueryResponseItem2(x)).ToArray();
            }
            catch (Exception ex)
            {
                _logger.LogError("Response parsing failed. Exception message: " + ex.Message);
                return null;
            }
        }

        public async Task<DrMaxFeed> GetStockCardsAsXML(string firmId)
        {
            var requestUri = $"query";

            var json = "{" +
                       " \"class\": \"storesubcards\"," +
                       " \"select\": [" +
                       "   \"store_id\"," +
                       "   \"storecard_id\"," +
                       "   \"store_id.Code\"," +
                       "   \"storecard_id.Code\"," +
                       "   \"storecard_id.Name\"," +
                       "   \"storecard_id.Ean\"," +
                       "   \"storecard_id.vatrate\"," +
                       "   \"storecard_id.intrastatcurrentprice\"," +
                       "   \"storecard_id.mainunitcode\"," +
                       "    {" +
                       "     \"name\": \"Reservations\"," +
                       "       \"value\": {" +
                       "       \"class\":\"reservations\"," +
                       "       \"select\": [{ \"name\": \"ReservedQTY\", \"value\": \"Sum(reserved)-Sum(supplied)\" }]," +
                       "       \"where\":\"storecard_id eq :storecard_id\"" +
                       "      }" +
                       "    }," +
                       "   \"Quantity\"" +
                       " ]," +
                      $" \"where\": \"store_id eq '2000000101' and storecard_id.hidden eq false and storecard_id.x_firm_id eq '{firmId}'\"" +
                       "}";


            //_logger.LogDebug("Sending request : " + json);
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(requestUri, httpContent);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError($"Request failed. ABRA response: {response.StatusCode}");
                return null;
            }
            try
            {
                var cards = await response.Content.ReadAsAsync<AbraQueryResponseItem[]>();
                return cards.ToDrMaxFeed();
            }
            catch (Exception ex)
            {
                _logger.LogError("Response parsing failed. Exception message: " + ex.Message);
                return null;
            }

        }
    }
}

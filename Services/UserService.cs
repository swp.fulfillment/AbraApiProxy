﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SwP.Fulfillment.AbraApiProxy.Models;
using SwP.Fulfillment.AbraApiProxy.Models.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwP.Fulfillment.AbraApiProxy.Services
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);
        Task<IEnumerable<User>> GetAll();
    }

    public class UserService : IUserService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<IUserService> _logger;
        public UserService(IConfiguration configuration, ILogger<IUserService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            _logger.LogDebug("Requesting users details");

            var users = _configuration.GetSection("DestApiUsers").Get<Models.User[]>();
            _logger.LogDebug("{0} users found", users.Count());

            var user = await Task.Run(() => users.SingleOrDefault(x => x.Username == username && x.Password == password));
            _logger.LogDebug("Checking agains U:{0} P:{1}", username, password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so return user details without password
            return user.WithoutPassword();
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            var users = _configuration.GetSection("DestApiUsers").Get<Models.User[]>();
            return await Task.Run(() => users.WithoutPasswords());
        }
    }
}
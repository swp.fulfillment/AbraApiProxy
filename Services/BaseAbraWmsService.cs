﻿using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using SwP.Fulfillment.AbraApiProxy.Models;

namespace SwP.Fulfillment.AbraApiProxy.Services
{
    public class BaseAbraWmsService : IDisposable
    {
        internal readonly HttpClient _client;
        internal readonly IConfiguration _configuration;

        public BaseAbraWmsService(IConfiguration configuration)
        {
            _configuration = configuration;
            var baseAddress = _configuration.GetValue<string>("BaseAddress");
            var user = _configuration.GetSection("SrcApiUser").Get<User>();
            _client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress),
                Timeout = TimeSpan.FromSeconds(5000)
            };
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var plainText = System.Text.Encoding.UTF8.GetBytes($"{user.Username}:{user.Password}");
            _client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(plainText));
        }

        public void Dispose()
        {
            _client.Dispose();
        }

    }
}

#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["SwP.Fulfillment.ReverseProxy.API/SwP.Fulfillment.ReverseProxy.API.csproj", "SwP.Fulfillment.ReverseProxy.API/"]
COPY ["SwP.Fulfillment.BusinessObjects/Swp.Fulfillment.BusinessObjects.csproj", "SwP.Fulfillment.BusinessObjects/"]
RUN dotnet restore "SwP.Fulfillment.ReverseProxy.API/SwP.Fulfillment.ReverseProxy.API.csproj"
COPY . .
WORKDIR "/src/SwP.Fulfillment.ReverseProxy.API"
RUN dotnet build "SwP.Fulfillment.ReverseProxy.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SwP.Fulfillment.ReverseProxy.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SwP.Fulfillment.ReverseProxy.API.dll"]

#docker build -f Dockerfile --force-rm -t luptak/swpfulfillmentapi:latest  --label "com.microsoft.created-by=visual-studio" --label "com.microsoft.visual-studio.project-name=SwP.Fulfillment.ReverseProxy.API" .
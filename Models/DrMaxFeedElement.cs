﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    [DataContract(Name = "element", Namespace = "")]
    [XmlRoot("element")]
    public class DrMaxFeedElement
    {
        [DataMember]
        public string EAN { get; set; }
        [DataMember]
        public string productId { get; set; }
        [DataMember]
        public int totalQuantity { get; set; }
        [DataMember]
        public double? price { get; set; }
        [DataMember]
        public double? vat { get; set; }
        //public Decimal moc { get; set; }
        //public string brand { get; set; }
        //public string category { get; set; }    
        //public string family { get; set; }
    }
}

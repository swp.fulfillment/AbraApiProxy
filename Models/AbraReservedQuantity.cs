﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class AbraReservedQuantity
    {
        [JsonProperty(PropertyName = "ReservedQTY")]
        public int Quantity { get; set; }
    }
}

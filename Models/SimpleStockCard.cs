﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class SimpleStockCard
    {
        [JsonProperty("KOD")]
        //[AbraWebApiFieldName("X_B2C_FirstName")]
        public string StoreCardCode { get; set; }
        [JsonProperty("NAZOV")]
        public string StoreCardName { get; set; }
        [JsonProperty("SKLAD")]
        public string StoreCode { get; set; }

        [JsonProperty("UNITQUANTITY")]
        public int Quantity { get; set; }
        [JsonProperty("QUNIT")]
        public string Unit { get; set; }
    }
}

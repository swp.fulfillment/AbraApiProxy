﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    [DataContract(Name = "root", Namespace = "")]
    [XmlRoot("root")]
    public class DrMaxFeed
    {
        [DataMember]
        [XmlArray("items")]
        [XmlArrayItem("element")]
        public List<DrMaxFeedElement> items { get; set; }
        [DataMember]
        [XmlElement]
        public DateTime timestamp { get; set; }
    }
}

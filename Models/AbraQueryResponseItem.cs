﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class AbraQueryResponseItem
    {
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        [JsonProperty(PropertyName = "storecard_id")]
        public string StoreCardId { get; set; }
        [JsonProperty(PropertyName = "store_id.Code")]
        public string StoreCode { get; set; }
        [JsonProperty(PropertyName = "storecard_id.code")]
        public string StoreCardCode { get; set; }
        [JsonProperty(PropertyName = "storecard_id.Name")]
        public string StoreCardName { get; set; }
        [JsonProperty(PropertyName = "storecard_id.Ean")]
        public string StoreCardEan { get; set; }
        [JsonProperty(PropertyName = "storecard_id.mainunitcode")]
        public string StoreCardUnit { get; set; }
        [JsonProperty(PropertyName = "Reservations")]
        public List<AbraReservedQuantity> ReservedQuantities { get; set; }
        [JsonProperty(PropertyName = "Quantity")]
        public int Quantity { get; set; }
        [JsonProperty(PropertyName = "storecard_id.IntrastatCurrentPrice")]
        public double? IntrastatCurrentPrice { get; set; }
        [JsonProperty(PropertyName = "storecard_id.VATRate")]
        public double? VATRate { get; set; }
    }
}

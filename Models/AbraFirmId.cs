﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class AbraFirmId
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class AbraQueryResponseItemDebug
    {
        [JsonProperty(PropertyName = "store_id")]
        public string StoreId { get; set; }
        [JsonProperty(PropertyName = "storecard_id")]
        public string StoreCardId { get; set; }
        [JsonProperty(PropertyName = "store_id.Code")]
        public string StoreCode { get; set; }
        [JsonProperty(PropertyName = "storecard_id.code")]
        public string StoreCardCode { get; set; }
        [JsonProperty(PropertyName = "storecard_id.Name")]
        public string StoreCardName { get; set; }
        [JsonProperty(PropertyName = "storecard_id.mainunitcode")]
        public string StoreCardUnit { get; set; }
        [JsonProperty(PropertyName = "Reservations")]
        public string ReservedQuantities { get; set; }
        [JsonProperty(PropertyName = "Quantity")]
        public string Quantity { get; set; }
    }
}

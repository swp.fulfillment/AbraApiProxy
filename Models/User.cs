﻿namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Domain { get; set; }
        public string Role { get; set; }
        public int SecurityLevel { get; set; }
        public int Id { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Password { get; set; }
    }
}

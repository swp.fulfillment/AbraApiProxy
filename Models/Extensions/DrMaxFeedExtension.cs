﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace SwP.Fulfillment.AbraApiProxy.Models.Extensions
{
    public static class DrMaxFeedExtension
    {
        public static DrMaxFeed ToDrMaxFeed(this AbraQueryResponseItem[] items)
        {
            var result = new DrMaxFeed
            {
                items = new List<DrMaxFeedElement>()
            };
            foreach (var item in items)
            {
                result.items.Add(
                    new DrMaxFeedElement
                    {
                        productId = item.StoreCardCode,
                        EAN = item.StoreCardEan,
                        totalQuantity = item.Quantity - item.ReservedQuantities.First().Quantity,
                        price = item.IntrastatCurrentPrice,
                        vat = item.VATRate
                    });

            };
            result.timestamp = DateTime.Now;
            return result;
        }
    }
}

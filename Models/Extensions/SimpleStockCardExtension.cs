﻿using System.Linq;

namespace SwP.Fulfillment.AbraApiProxy.Models.Extensions
{
    public static class SimpleStockCardExtension
    {
        public static SimpleStockCard FromAbraQueryResponseItem(AbraQueryResponseItem i)
        {
            return new SimpleStockCard
            {
                StoreCardCode = i.StoreCardCode,
                StoreCardName = i.StoreCardName,
                StoreCode = i.StoreCode,
                Unit = i.StoreCardUnit,
                Quantity = i.Quantity - i.ReservedQuantities.First().Quantity,
            };
        }
        public static SimpleStockCard2 FromAbraQueryResponseItem2(AbraQueryResponseItem i)
        {
            return new SimpleStockCard2
            {
                nazov = i.StoreCardName,
                kod = i.StoreCardCode,
                sklad = i.StoreCode,
                qunit = i.StoreCardUnit,
                unitquantity = i.Quantity - i.ReservedQuantities.First().Quantity,
            };
        }
    }
}

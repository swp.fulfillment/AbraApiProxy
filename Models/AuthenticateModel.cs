﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}

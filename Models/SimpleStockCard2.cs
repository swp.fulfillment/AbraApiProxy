﻿using Newtonsoft.Json;

namespace SwP.Fulfillment.AbraApiProxy.Models
{
    public class SimpleStockCard2
    {
        [JsonProperty("KOD")]
        //[AbraWebApiFieldName("X_B2C_FirstName")]
        public string kod { get; set; }
        [JsonProperty("NAZOV")]
        public string nazov { get; set; }
        [JsonProperty("SKLAD")]
        public string sklad { get; set; }

        [JsonProperty("UNITQUANTITY")]
        public int unitquantity { get; set; }
        [JsonProperty("QUNIT")]
        public string qunit { get; set; }
    }
}

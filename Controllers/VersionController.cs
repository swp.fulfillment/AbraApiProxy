﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.Threading.Tasks;

namespace SwP.Fulfillment.AbraApiProxy.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VersionController : ControllerBase
    {
        private readonly ILogger<VersionController> _logger;
        public VersionController(ILogger<VersionController> logger)
        {
            _logger = logger;
        }
        [HttpGet]
        public async Task<ActionResult<string>> Get()
        {
            return await Task.FromResult(Ok(Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion));
        }
    }
}

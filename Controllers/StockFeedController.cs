﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwP.Fulfillment.AbraApiProxy.Services;
using System.Threading.Tasks;

namespace SwP.Fulfillment.AbraApiProxy.Controllers
{
    [ApiController]
    [FormatFilter]
    [Route("data/[controller]")]
    public class StockFeedController : ControllerBase
    {
        private readonly IProxyService _abraWmsService;
        private readonly ILogger<StockController> _logger;

        public StockFeedController(ILogger<StockController> logger, IProxyService abraWmsService)
        {
            _abraWmsService = abraWmsService;
            _logger = logger;
        }
        [Route("{firmid}.{format}")]
        [HttpGet]
        public async Task<ActionResult> Get(string firmid)
        {
            _logger.LogDebug("Request for stock listing received");
            try
            {
                if (string.IsNullOrEmpty(firmid))
                {
                    _logger.LogError("GET: Client Id not recognized");
                    return StatusCode(StatusCodes.Status403Forbidden, "APIKEY not recognized");
                }
                return Ok(await _abraWmsService.GetStockCardsAsXML(firmid));
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Captured exception GET");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
    }

}

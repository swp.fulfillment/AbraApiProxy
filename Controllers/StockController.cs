﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwP.Fulfillment.AbraApiProxy.Services;
using System.Threading.Tasks;

namespace SwP.Fulfillment.AbraApiProxy.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StockController : ControllerBase
    {
        private readonly IProxyService _abraWmsService;
        private readonly ILogger<StockController> _logger;

        public StockController(ILogger<StockController> logger, IProxyService abraWmsService)
        {
            _abraWmsService = abraWmsService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult> Get([FromQuery] string APIKEY, [FromQuery] string DataDefinition)
        {
            _logger.LogDebug("Request for order listing received");
            try
            {
                if (string.IsNullOrEmpty(DataDefinition) || !DataDefinition.Equals("B2B_Info2"))
                {
                    _logger.LogError("GET: Unknown data definiton");
                    return StatusCode(StatusCodes.Status404NotFound, "Unknown data definition");
                }
                if (string.IsNullOrEmpty(APIKEY))
                {
                    _logger.LogError("GET: APIKEY missing");
                    return StatusCode(StatusCodes.Status404NotFound, "APIKEY missing");
                }
                var firmId = await _abraWmsService.GetIdFromApikey(APIKEY);

                if (firmId == null || string.IsNullOrEmpty(firmId))
                {
                    _logger.LogError("GET: APIKEY not recognized");
                    return StatusCode(StatusCodes.Status403Forbidden, "APIKEY not recognized");
                }
                return Ok(await _abraWmsService.GetStockCards(firmId));
            }
            catch (System.Exception e)
            {
                _logger.LogError(e, "Captured exception GET");
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }
    }

}

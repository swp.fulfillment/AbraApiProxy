﻿using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Net;
using System;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SwP.Fulfillment.AbraApiProxy
{
    public class IgnoreNamespacesXmlMediaTypeFormatter : XmlMediaTypeFormatter
    {
        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            try
            {
                var task = Task.Factory.StartNew(() =>
                {
                    var xns = new XmlSerializerNamespaces();
                    var serializer = new XmlSerializer(type);
                    xns.Add(string.Empty, string.Empty);
                    serializer.Serialize(writeStream, value, xns);
                });

                return task;
            }
            catch (Exception)
            {
                return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
            }
        }
    }
}
